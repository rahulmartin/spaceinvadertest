﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;

public class TakeAscent : Action
{
    public SharedVector3 targetPosition;
    public SharedBossProperty property;
    bool hasAscended = false;
    bool isAscending = true;

    public override void OnStart() {
        hasAscended = false;
        isAscending = true;
    }

    public override TaskStatus OnUpdate() {
        if (hasAscended)
            return TaskStatus.Success;

        if(isAscending) {
            transform.position = Vector3.MoveTowards(
                transform.position,
                targetPosition.Value,
                (property.Value.moveSpeed/3f) * Time.deltaTime
                );

            if(Vector3.Distance(transform.position, targetPosition.Value)< 0.01f) {
                isAscending = false;
                hasAscended = true;
            }
        }

        return TaskStatus.Running;
    }
}
