﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvadersGameSpace;
using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedBossProperty : SharedVariable<BossProperties> 
{
    public static implicit operator SharedBossProperty(BossProperties value) { return new SharedBossProperty { Value = value }; }

}