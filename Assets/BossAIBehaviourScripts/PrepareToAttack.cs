﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;

public class PrepareToAttack : Action
{
    public SharedBossProperty property;
    bool isMovingToPosition = false;
    bool hasMoved = false;
    public SharedGameObject selectedPlayer;
    public override void OnStart() {
        isMovingToPosition = false;
        hasMoved = false;
        int numberOfPlayers = DataProvider.instance.GetNumberOfPlayers();
        List<PlayerController> allPlayers = GameManager.instance.playerManager.instantiatedPlayers;
        List<PlayerController> attackablePlayers = new List<PlayerController>();
        attackablePlayers.AddRange(allPlayers);
        for(int i=0; i<allPlayers.Count; i++) {
            if (allPlayers[i])
                if (!allPlayers[i].gameObject.activeInHierarchy)
                    attackablePlayers.Remove(allPlayers[i]);
        }

        if(attackablePlayers.Count > 0)
            selectedPlayer.Value = attackablePlayers[Random.Range(0, attackablePlayers.Count)].gameObject;
        
        isMovingToPosition = true;
    }

    public override TaskStatus OnUpdate() {
        if (hasMoved)
            return TaskStatus.Success;
        if(isMovingToPosition) {
            Vector3 destination = new Vector3(selectedPlayer.Value.transform.position.x, transform.position.y,
                transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, 
                destination, property.Value.moveSpeed/2 * Time.deltaTime);

            if(Vector3.Distance(transform.position, destination) < 0.1f) {
                isMovingToPosition = false;
                hasMoved = true;
            }
        }

        return TaskStatus.Running;
    }

}
