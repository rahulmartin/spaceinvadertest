﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner;

public class IsHpAboveFirstThreshold : Conditional
{
    bool isHpAbovefirstThreshold;
    public SharedBossProperty bossProperty;
    public override void OnStart() {
        float hpFirstThreshold = 70;
        if (bossProperty.Value.HP > hpFirstThreshold)
            isHpAbovefirstThreshold = true;
        else
            isHpAbovefirstThreshold = false;
    }

    public override TaskStatus OnUpdate() {
        if (isHpAbovefirstThreshold)
            return TaskStatus.Success;
        else
            return TaskStatus.Failure;
    }
}
