﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;

public class DropToPlayers : Action
{
    bool hasDropped = false;
    bool isDropping = true;
    public SharedGameObject playerToAttack;
    public SharedBossProperty property;
    public SharedVector3 initialElevation;
    public override void OnStart() {
        hasDropped = false;
        isDropping = true;
        initialElevation.Value = transform.position;
    }

    public override TaskStatus OnUpdate() {
        if (hasDropped)
            return TaskStatus.Success;
        if(isDropping) {
            Vector3 destination = new Vector3(
                transform.position.x,
                playerToAttack.Value.transform.position.y,
                transform.position.z
                );
            transform.position = Vector3.MoveTowards(transform.position, destination, property.Value.moveSpeed*Time.deltaTime);

            if (Vector3.Distance(transform.position, destination) < 0.25f) {
                hasDropped = true;
                isDropping = false;
            }
        }

        return TaskStatus.Running;
    }

}
