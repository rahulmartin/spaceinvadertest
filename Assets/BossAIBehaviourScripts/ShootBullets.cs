﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using SpaceInvadersGameSpace;

public class ShootBullets : Action
{
    bool isShootingDone = false;
    bool isShooting = false;
    float timeSinceLastShoot = 0f;
    float shootDelay = 0.25f;
    int burstCount = 3;
    public override void OnStart() {
        isShootingDone = false;
        isShooting = true;
        timeSinceLastShoot = 0;
        burstCount = Random.Range(0,4);
    }

    public override TaskStatus OnUpdate() {
        if (isShootingDone)
            return TaskStatus.Success;

        if(isShooting) {
            if (burstCount > 0) {
                timeSinceLastShoot -= Time.deltaTime;
                if (timeSinceLastShoot <= 0) {
                    Shoot();
                    timeSinceLastShoot = shootDelay;
                    burstCount--;
                }
            }

            if (burstCount <= 0) {
                isShootingDone = true;
                isShooting = false;
            }
        }

        return TaskStatus.Running;
    }

    private void Shoot() {
        GameObject bullet = GameManager.instance.GetPooledObject(POOLABLES.ENEMYBULLET);
        EnemyBullet bulletController = bullet.GetComponent<EnemyBullet>();
        bullet.transform.position = transform.position;
        bulletController.Shoot(OnBulletDestroyed);
        bullet.SetActive(true);
    }

    private void OnBulletDestroyed() {

    }
}
