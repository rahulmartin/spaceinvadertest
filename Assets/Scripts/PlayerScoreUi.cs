﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using SpaceInvadersGameSpace;

/// <summary>
/// Propvides methods to change player score and respawns values
/// </summary>
public class PlayerScoreUi : MonoBehaviour
{
    private int playerID;
    public Text playerNameText;
    public Text playerScore;
    public Text respawns;

    /// <summary>
    /// Initializes a player score ui
    /// </summary>
    /// <param name="playerID"></param>
    public void Init(int playerID) {
        PlayerProperties prop = GameManager.instance.playerManager.GetPlayer(playerID).properties;
        this.playerID = playerID;
        this.playerNameText.text ="Player " + playerID;
        this.playerScore.text = "0";
        this.respawns.text = "HP:" + prop.respawns;
        prop.RegisterScoreUpdateCallback(OnScoreUpdated);
    }

    /// <summary>
    /// callback method for when scores or respawn is updated
    /// </summary>
    /// <param name="score"></param>
    /// <param name="res"></param>
    public void OnScoreUpdated(int score, int res) {
        playerScore.text = score.ToString();
        respawns.text = "HP:" + res.ToString();
    }
}
