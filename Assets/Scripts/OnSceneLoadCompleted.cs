﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This object must persist in a game scene, to notify Gamemanager when to start the game
/// </summary>
public class OnSceneLoadCompleted : MonoBehaviour
{
    //Notifies game manager that the scene is ready to be played
    //and all awake calls on objects are already run by this time
    public void Start() {
        GameManager.instance.OnSceneLoadCompleted();
    }
}
