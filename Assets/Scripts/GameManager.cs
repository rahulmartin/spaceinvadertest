﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvadersGameSpace;

/// <summary>
/// Game manager works as the game Initiator, it deals with high level logic
/// and as a provider of other game entities like object pooler , players manager
/// enemcontroller.
/// </summary>
public class GameManager : MonoBehaviour {

    public static GameManager instance;
    [HideInInspector]
    public ObjectPooler objectPooler;
    [HideInInspector]
    public EnemyControllerType enemyController;
    [HideInInspector]
    public PlayersManager playerManager;
    [HideInInspector]
    private PlayerUIController playerUiContainer;
    [HideInInspector]
    private GameEndUIController GameEndUi;
    [HideInInspector]
    private BlockerSpawner blockManager;

    public bool isGameStarted {
        get { return _isGameStarted; }
    }

    private bool _isGameStarted = false;

    // Setting up the singleton
    void Awake() {
        if (instance != null)
            DestroyImmediate(this);
        else {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    //Moves the game from Base scene to menu scene
    public void Start() {
        LoadLevel("MenuScene");
        AudioManager.instance.PlayMusic(AUDIOTYPE.MUSIC);
    }

    //Debugging Code to fast test reset and back to menu
    
    public void Update() {
        if (Input.GetKeyDown(KeyCode.R)) {
            ResetGame();
        }

        if(Input.GetKeyDown(KeyCode.U)) {
            LoadLevel("MenuScene");
        }
    }

    //This method starts off the game when scene loading completes
    public void OnSceneLoadCompleted() {
        FetchAllReferences();
        ResetGame();
    }

    //Fetch all required references to start a game
    private void FetchAllReferences() {
        objectPooler = FindObjectOfType<ObjectPooler>();
        if (!objectPooler)
            Debug.LogError("You forget to add object pooler to game scene");
        enemyController = FindObjectOfType<EnemyControllerType>();
        if (!enemyController)
            Debug.LogError("enemyController is not found in game scene");

        playerManager = FindObjectOfType<PlayersManager>();
        if (!playerManager)
            Debug.LogError("Player manager was not found");

        playerUiContainer = FindObjectOfType<PlayerUIController>();
        if (!playerUiContainer)
            Debug.LogError("Ui was unavailable for players");

        GameEndUi = FindObjectOfType<GameEndUIController>();
        if (!GameEndUi)
            Debug.LogError("Game End Ui was not found in scene");

        blockManager = FindObjectOfType<BlockerSpawner>();
        if (!blockManager)
            Debug.Log("blocker dont exist");
    }

    //Resets and starts the game
    private void ResetGame() {
        _isGameStarted = false;
        objectPooler.InitObjectPooler();
        enemyController.InitEnemyController();
        playerManager.InitPlayerManager(DataProvider.instance.GetNumberOfPlayers());
        playerUiContainer.SetUpUiFor(DataProvider.instance.GetNumberOfPlayers());
        GameEndUi.gameObject.SetActive(false);
        blockManager.InitBlockerSpawner();
        _isGameStarted = true;
    }

    //This method is called when enemy win by moving vertically
    public void OnEnemyWonByAssension() {
        _isGameStarted = false;
        OnGameEnd("Invaders Won");
        enemyController.OnGameEnded();
    }

    //This method is called when a player is destroyed
    public void OnPlayerDestroyed(int playerID) {
        if (!playerManager.AreAllPlayersDead())
            return;
        _isGameStarted = false;
        OnGameEnd("Invaders Won");
        enemyController.OnGameEnded();
    }

    //This method is called when player destroyes all enemies
    public void OnPlayerWon() {
        _isGameStarted = false;
        OnGameEnd("Defenders Won");
        enemyController.OnGameEnded();
    }

    //Shows the Game End Ui
    private void OnGameEnd(string text) {
        GameEndUi.SetText(text);
        GameEndUi.gameObject.SetActive(true);
    }

    //Replay the game on button click
    public void OnReplayClicked() {
        ResetGame();
    }

    #region Public Methods for Accessing Game Features

    //Object pooler is accessible from here
    public GameObject GetPooledObject(POOLABLES item) {
        return objectPooler.GetPooledObject(item);
    }

    //Level changing method
    public void LoadLevel(string levelName) {
        UnityEngine.SceneManagement.SceneManager.LoadScene(levelName);
    }

    #endregion
}
