﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvadersGameSpace {
    /* I am going to use serialized calsses for saving data for differnt
     * systems. Doing so makes it easier to reset data, as if we create many
     * variables in monobehaviours its harder to reset each and everyone by hand,
     * here we will just create a new object.
     * 
     * Also having serialized classes enables us to export them into json or other
     * format and we can easily save them locally of on cloud.
     * 
     * We make sure that data is never null, we always have base data
     * 
     * Deep copy is simpler for bigger classes as we can just ser/deserialize them
     * and get a new copy
     *  
     *  serialized classes are editable on Unity Editor and we let designers to work
     *  on editor and not on our code base
     */
    [System.Serializable]
    /// <summary>
    /// Input config for players
    /// </summary>
    public class InputConfig {
        public KeyCode leftKey;
        public KeyCode rightKey;
        public KeyCode shootKey;
    }

    [System.Serializable]
    //Character properties that can exist in both enemy and players
    public class CharacterProperties {
        //Movement speed
        [Range(1, 10)]
        public float moveSpeed;

        //Fire rate in this game is on screen bullets at a time.
        [Range(1, 10)]
        public float fireRate;

        public Vector3 shootDirection;
    }

    [System.Serializable]
    //Players properties 
    public class PlayerProperties : CharacterProperties {
        public int playerID;
        public int respawns {
            get { return _respawns; }
            set {
                _respawns = value;
                onScoreUpdated(_score, value);
            }
        }
        public int score {
            get { return _score; }
            set { _score = value;
                onScoreUpdated(value, _respawns);
            }
        }
        private int _score;
        private int _respawns;

        UnityEngine.Events.UnityAction<int, int> onScoreUpdated;

        //register callback for score and respawn updation for ui
        public void RegisterScoreUpdateCallback(UnityEngine.Events.UnityAction<int,int> method) {
            onScoreUpdated += method;
        }

        //resets callbacks
        public void ResetCallbacks() {
            onScoreUpdated = null;
        }

        //deep copy here, we can also serialize deserialize for deep copy of bigger classes
        //but this will do for this game
        public PlayerProperties(PlayerProperties origional) {
            moveSpeed = origional.moveSpeed;
            shootDirection = origional.shootDirection;
            fireRate = origional.fireRate;
            _score = 0;
            _respawns = 3;
            ResetCallbacks();
        }
    }

    //Enemies required properties
    [System.Serializable]
    public class EnemyProperties : CharacterProperties {
        public Vector2Int matrixLocation;
        public int rewardPoints;

        public EnemyProperties() {

        }

        public EnemyProperties(EnemyProperties origional, Vector2Int matLoc) {
            moveSpeed = origional.moveSpeed;
            shootDirection = origional.shootDirection;
            fireRate = origional.fireRate;
            rewardPoints = origional.rewardPoints;
            matrixLocation = matLoc;
        }
    }

    [System.Serializable]
    public class BossProperties : EnemyProperties {
        public int HP;
    }

    //Base class for bullet properties
    [System.Serializable]
    public class BulletBase {
        public float moveSpeed;
        [HideInInspector]
        public Vector3 moveDirection;
        public float destroyDelay;
    }

    //Player bullet properties
    [System.Serializable]
    public class PlayerBulletProperties : BulletBase {
        [HideInInspector]
        public int shooterID;
    }

    //Enemy Bullet Properties
    [System.Serializable]
    public class EnemyBulletProperties : BulletBase {
    }

    //Game space data
    [System.Serializable]
    public class GameInternalProperties {
        public float horizontalSpace;
        public float verticalSpace;
    }

    //Game Mode Data
    [System.Serializable]
    public class LevelEnemyData {
        public float horizontalDistance = 1f;
        public float verticalDistance = 0.25f;
        public float horizontalMoveStep = 0.25f;
        public float verticalMoveStep = 0.5f;
        public float moveTimeDelay = 0.25f;
        public int maxColumns = 5;
        public int enemyPerColumns = 5;
        [Tooltip("How many bullets on screen at a time")]
        public int fireRate = 3;
        [Tooltip("How Much Y movement of enemy will make game loose")]
        public float verticalLooseLimit = -1.5f;
    }

}