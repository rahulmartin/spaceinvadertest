﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvadersGameSpace;

//Our Base class InputProvider is an Abstract class
//This has been done to keep the Player Input Controller to be open to any future extensions.
//While the basic functions of movement are already taken care of in the abstraction
public class PlayerController : InputProvider {

	//Highly Coupled memebers
    [HideInInspector]
	public PlayerProperties properties;
	private int firedBullets;
    private bool isPlayerInactive = false;

	#region implemented abstract members of InputProvider

	//As we need to clamp move for players, we will override this method
	public override void OnLeftKeyDown() {
		MovePlayer(Vector3.left);
	}

	//As we need to clamp move for players, we will override this method
	public override void OnRightKeyDown() {
		MovePlayer(Vector3.right);
	}

	//Implementing abstract shoot method
	public override void OnShootKeyDown() {
		SpawnBullet();
	}

	#endregion

	//Moves player in given direction, with world space clamping
	public void MovePlayer(Vector3 direction) {
        if (!GameManager.instance.isGameStarted)
            return;
        if (isPlayerInactive)
            return;
		float horizontalLimit = DataProvider.instance.GetHorizontalRange();

		//Getting clamped X position of the object
		float clampedX = Mathf.Clamp(transform.position.x +(direction.x*properties.moveSpeed*Time.deltaTime), 
			horizontalLimit*-1,
			horizontalLimit);

		//Shifting to clamped position is better than translating to position and then clamping
		transform.position = new Vector3(clampedX, transform.position.y, transform.position.z);
	}

	//Spawns a bullet and sets its initial position and launch it
	public void SpawnBullet() {
        if (!GameManager.instance.isGameStarted)
            return;
        if (isPlayerInactive)
            return;
        //check if fire rate is already met, return
        if (!CanFireBullet())
			return;
		
		//Fetch a bullet from pooler
		GameObject bullet = GameManager.instance.GetPooledObject(POOLABLES.PLAYERBULLET);
		if(bullet == null) {
			//cannot create more bullets
			return;
		}

		//Code to launch the bullet
		bullet.transform.position = this.transform.position;
		PlayerBullet bulletController = bullet.GetComponent<PlayerBullet>();
		bulletController.Shoot(properties, OnBulletDestroyed);
		bullet.SetActive(true);
		firedBullets++;
	}

	public void OnBulletDestroyed() {
		firedBullets--;
	}

	private bool CanFireBullet() {
		return firedBullets < properties.fireRate;
	}

    //When a Enemy Bullet collides with this Player, this method will be executed
    void OnTriggerEnter2D(Collider2D col) {
        if (!GameManager.instance.isGameStarted)
            return;
        if (isPlayerInactive)
            return;
        //Notify bullet that it has been collided
        EnemyBullet bullet = col.GetComponent<EnemyBullet>();

        //Only take one collision at a time
        if(bullet.isPropertySet)
        bullet.OnCollidedWithPlayer(properties.playerID);

        properties.respawns -= 1;

        if(properties.respawns < 0) {
            DestroyPlayer();
        } else {
            StartCoroutine(DisablePlayerForSeconds(1f));
        }

        AudioManager.instance.PlayAudio(AUDIOTYPE.PLAYERHIT);
    }
    
    void DestroyPlayer() {
        GameManager.instance.OnPlayerDestroyed(properties.playerID);
        this.gameObject.SetActive(false);
    }

    IEnumerator DisablePlayerForSeconds(float time) {
        SpriteRenderer spRend = GetComponent<SpriteRenderer>();
        Color baseColor = spRend.color;
        Color fadeColor = new Color(baseColor.r, baseColor.g, baseColor.b, baseColor.a / 2f);
        spRend.color = fadeColor;
        isPlayerInactive = true;
        yield return new WaitForSeconds(time);
        spRend.color = baseColor;
        isPlayerInactive = false;
    }
}
