﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using SpaceInvadersGameSpace;
/// <summary>
/// Enemy controller is component attached to each enemy in scene, this is required
/// for getting collision calls and other enemy properties requirement for different
/// systems.
/// </summary>
public class EnemyController : MonoBehaviour {

	public EnemyProperties enemyProperty;
	public UnityAction<Vector2Int> OnKilledCallback;

	//Set enemy data
	public void InitEnemy(int enemyType, Vector2Int _matLoc) {
        enemyProperty = DataProvider.instance.GetEnemyPropertyFor(enemyType, _matLoc);
        enemyProperty.matrixLocation = _matLoc;
	}

	//Register kill callback
	public void RegisterOnKillCallback(UnityAction<Vector2Int>  _killCallback) {
		OnKilledCallback += _killCallback;
	}

    //When a Player Bullet collides with this enemy, this method will be executed
	void OnTriggerEnter2D(Collider2D col)
	{
        if (enemyProperty == null)
            return;
        //Notify bullet that it has been collided
		PlayerBullet bullet = col.GetComponent<PlayerBullet>();
		bullet.OnCollidedWithEnemy(enemyProperty.rewardPoints);

        AudioManager.instance.PlayAudio(AUDIOTYPE.ENEMYHIT);
        //Inovke callback method for registered listeners
		OnKilledCallback(enemyProperty.matrixLocation);
		Remove();
	}

    //Reset the data and remove from active view
	public void Remove() {
		enemyProperty = null;
		OnKilledCallback = null;
		gameObject.SetActive(false);
	}
}
