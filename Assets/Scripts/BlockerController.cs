﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockerController : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        EnemyBullet bullet = col.GetComponent<EnemyBullet>();
        if(bullet != null)
            if(bullet.isPropertySet)
                bullet.Remove();

        PlayerBullet pbullet = col.GetComponent<PlayerBullet>();
        if (pbullet != null)
            if (pbullet.propertySet)
                pbullet.Remove();
        this.gameObject.SetActive(false);
    }
}
