﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A class to handle enabling disabling of differnt compopnents in scene
/// </summary>
public class MainMenuController : MonoBehaviour
{
    public MenuGroupController menuGroupController;
    public GameObject configGroupController;

    /// <summary>
    /// Enables menu group and disables config group
    /// </summary>
    public void EnableMenuGroupController() {
        configGroupController.gameObject.SetActive(false);
        menuGroupController.gameObject.SetActive(true);
    }

    /// <summary>
    /// Enables config group and disables menu group
    /// </summary>
    public void EnableConfigMenu() {
        configGroupController.gameObject.SetActive(true);
        menuGroupController.gameObject.SetActive(false);
    }
}
