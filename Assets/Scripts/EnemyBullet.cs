﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvadersGameSpace;
using UnityEngine.Events;

/// <summary>
/// Enemy Bullet behaviour
/// </summary>
public class EnemyBullet : MonoBehaviour
{
    public BulletBase baseData;
    public BulletBase runtimeProperty;
    private UnityAction onDestroyCallback;
    public bool isPropertySet = false;

    //Called to initiallize a bullet and make it start moving
    public void Shoot(UnityAction _onDestroyCallback, float moveSpeed = 0) {
        runtimeProperty = new BulletBase {
            moveDirection = Vector3.down,
            moveSpeed = baseData.moveSpeed,
            destroyDelay = baseData.destroyDelay
        };

        if (moveSpeed > 0)
            runtimeProperty.moveSpeed = moveSpeed;
        onDestroyCallback = _onDestroyCallback;
        isPropertySet = true;

        AudioManager.instance.PlayAudio(AUDIOTYPE.ENEMYBULLET);
    }

    //Moves the bullet and removes after delay time expires
    public void Update() {
        if (!isPropertySet)
            return;

        transform.position = new Vector3(
            transform.position.x,
            transform.position.y + (runtimeProperty.moveDirection.y * Time.deltaTime * runtimeProperty.moveSpeed),
            transform.position.z
            );

        if (runtimeProperty.destroyDelay > 0)
            runtimeProperty.destroyDelay -= Time.deltaTime;
        else
            Remove();
    }

    //callbakc for when bullet hits a player
    public void OnCollidedWithPlayer(int playerID) {
        Remove();
    }

    //Remove bullet and make it ready for pooler
    public void Remove() {
        isPropertySet = false;
        onDestroyCallback();
        onDestroyCallback = null;
        gameObject.SetActive(false);
    }
}
