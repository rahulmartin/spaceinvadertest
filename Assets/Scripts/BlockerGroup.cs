﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockerGroup : MonoBehaviour
{
    public List<GameObject> fragments = new List<GameObject>();

    public void Init() {
        for(int i=0; i<fragments.Count; i++) {
            fragments[i].SetActive(true);
        }
    }
}
