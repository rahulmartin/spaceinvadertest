﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvadersGameSpace {
    /// <summary>
    /// Input Provider provides input to all players with different key configurations
    /// it implements input interface to carry forward the key press event to other systems
    /// The goal is to have single code for all player controllers , for accepting different
    /// inputs.
    /// </summary>
	public abstract class InputProvider : MonoBehaviour, InputInterface {

		//Highly Coupled Required Members
		public InputConfig inputConfig;

        //Input taking for a given input configuration
		public virtual void Update() {
			if(Input.GetKey(inputConfig.leftKey)) {
				OnLeftKeyDown();
			}

			if(Input.GetKey(inputConfig.rightKey)) {
				OnRightKeyDown();
			} 
				
			if(Input.GetKeyDown(inputConfig.shootKey)) {
				OnShootKeyDown();
			}
		}

		#region InputInterface implementation

		//Default virtual method for moving object Left
		public virtual void OnLeftKeyDown() {
			//As this could be on any entity we need a basic move speed for default behaviour
			float baseMoveSpeed = 0.5f;

			//Move object left without clamping
			transform.Translate(Vector3.left* baseMoveSpeed * Time.deltaTime);
		}

		//Default virtual method for moving object right
		public virtual void OnRightKeyDown() {
			//As this could be on any entity we need a basic move speed for default behaviour
			float baseMoveSpeed = 0.5f;

			//Move object right without clamping
			transform.Translate(Vector3.right* baseMoveSpeed *Time.deltaTime);
		}

		//Shooting is Character specific command so we leave it as is
		public abstract void OnShootKeyDown();

		#endregion
	}

	/// <summary>
	/// Input interface, all input taking should Implement this
	/// </summary>
	public interface InputInterface {
		void OnLeftKeyDown();
		void OnRightKeyDown();
		void OnShootKeyDown();
	}
}
