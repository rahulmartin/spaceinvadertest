﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvadersGameSpace;
using UnityEngine.UI;
using BehaviorDesigner.Runtime;

public class BossController : EnemyControllerType {

    BossProperties bossProperty;
    public GameObject bossObject;
    public Image HpUi;
    Vector3 oldScaleVal;

    public override void InitEnemyController() {
        BehaviorTree bTree = bossObject.GetComponent<BehaviorTree>();
        bossProperty = new BossProperties();
        bossProperty.HP = 100;
        bossProperty.rewardPoints = 15;
        bossProperty.moveSpeed = 15f;
        bossObject.SetActive(true);
        HpUi.fillAmount = 1;
        oldScaleVal = bossObject.transform.localScale;
        bossObject.transform.localScale = new Vector3(4,4,4);
        bossObject.transform.position = new Vector3(-7.27f,2f,0f);
        //SetUp all values for AI
        SharedBossProperty property = new SharedBossProperty();
        property.Value = bossProperty;
        bTree.SetVariable("BossProperty", property);
        bTree.EnableBehavior();
    }

    public void OnBulletCollided(Collider2D col) {
        bossProperty.HP -= 1;

        col.GetComponent<PlayerBullet>().
            OnCollidedWithEnemy(bossProperty.rewardPoints);

        if (bossProperty.HP <= 0)
            GameManager.instance.OnPlayerWon();
        HpUi.fillAmount = bossProperty.HP / 100f;
        ReduceScale();

        AudioManager.instance.PlayAudio(AUDIOTYPE.ENEMYHIT);
    }

    void ReduceScale() {
        bossObject.transform.localScale = new Vector3(
    bossObject.transform.localScale.x - oldScaleVal.x * 0.008f,
     bossObject.transform.localScale.y - oldScaleVal.y * 0.008f,
      bossObject.transform.localScale.z - oldScaleVal.z * 0.008f
    );
    }

    void Remove() {
        bossObject.SetActive(false);
    }

    public override void OnGameEnded() {
        BehaviorTree bTree = bossObject.GetComponent<BehaviorTree>();
        bossObject.transform.position = new Vector3(-7.27f, 2f, 0f);
        bTree.DisableBehavior();
    }
}
