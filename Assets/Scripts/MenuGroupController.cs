﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to handle menu group inputs
/// </summary>
public class MenuGroupController : MonoBehaviour {

    /// <summary>
    /// Make game ready for classic mode and moves to game scene
    /// </summary>
    /// <param name="numberOfPlayers"></param>
    public void ClassicModeClicked(int numberOfPlayers) {
        DataProvider.instance.SetNextLevelData(numberOfPlayers, GAMEMODE.CLASSIC);
        GameManager.instance.LoadLevel("GameScene");
    }

    /// <summary>
    /// Makes game ready for Hyper mode and moves to game scene
    /// </summary>
    /// <param name="numberOfPlayers"></param>
    public void HyperModeClicked(int numberOfPlayers) {
        DataProvider.instance.SetNextLevelData(numberOfPlayers, GAMEMODE.HYPER);
        GameManager.instance.LoadLevel("GameScene");
    }

    /// <summary>
    /// Makes the game ready for boss mode and moves to Boss scene
    /// </summary>
    /// <param name="numberOfPlayers"></param>
    public void BossModeClicked(int numberOfPlayers) {
        DataProvider.instance.SetNextLevelData(numberOfPlayers, GAMEMODE.BOSS);
        GameManager.instance.LoadLevel("BossScene");
    }
}
