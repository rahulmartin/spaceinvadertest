﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockerSpawner : MonoBehaviour
{
    public List<GameObject> blockerPositions;

    public void InitBlockerSpawner() {
        for(int i=0; i<blockerPositions.Count; i++) {
            GameObject pooledObject = GameManager.instance.GetPooledObject(SpaceInvadersGameSpace.POOLABLES.BLOCKER);
            pooledObject.transform.position = blockerPositions[i].transform.position;
            pooledObject.GetComponent<BlockerGroup>().Init();
            pooledObject.SetActive(true);
        }
    }
}
