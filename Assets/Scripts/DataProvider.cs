﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvadersGameSpace;

/// <summary>
/// Dataprovider is a singleton that works as a provider for global values. data is
/// easier to manage if it has a single point of origin. Dataprovider here provides
/// gamespace data, selected game mode, level data and player configuration for 
/// selected game mode.
/// </summary>
public class DataProvider : MonoBehaviour {

    public static DataProvider instance;
    [SerializeField]
    private GameInternalProperties gameSpace;
    private LevelData selectedLevelData;
    public LevelDataContainer levelContainer;
    public List<InputConfig> playersInputConfiguration;

    private int numberOfPlayers;

    /// <summary>
    /// Setting up singleton
    /// </summary>
    public void Awake() {
        if (instance != null)
            DestroyImmediate(this);
        else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    //returns Horizontal move space for enemies and player
    public float GetHorizontalRange() {
        return gameSpace.horizontalSpace;
    }

    //returns vertcal move space
    public float GetVerticalRange() {
        return gameSpace.verticalSpace;
    }

    /// <summary>
    /// Returns enemy data for selected mode
    /// </summary>
    /// <returns></returns>
    public LevelEnemyData GetEnemyData() {
        return selectedLevelData.enemyData;
    }

    /// <summary>
    /// returns enemy property for a given enemy type in a matrix location
    /// </summary>
    /// <param name="enemyType"></param>
    /// <param name="matLoc"></param>
    /// <returns></returns>
    public EnemyProperties GetEnemyPropertyFor(int enemyType, Vector2Int matLoc) {
        EnemyProperties newProperty = new EnemyProperties(
            selectedLevelData.enemyPropertyList[enemyType], matLoc);
        return newProperty;
    }

    /// <summary>
    /// returns player property for given game mode
    /// </summary>
    /// <param name="playerID"></param>
    /// <returns></returns>
    public PlayerProperties GetPlayerPropertyFor(int playerID) {
        PlayerProperties newProperty = new PlayerProperties(
            selectedLevelData.playerProperties[playerID]);
        return newProperty;
    }

    /// <summary>
    /// returns number of players in game
    /// </summary>
    /// <returns></returns>
    public int GetNumberOfPlayers() {
        return numberOfPlayers;
    }

    /// <summary>
    /// selects the data for the next upcoming level
    /// </summary>
    /// <param name="_numberOfPlayers"></param>
    /// <param name="mode"></param>
    public void SetNextLevelData(int _numberOfPlayers, GAMEMODE mode) {
        numberOfPlayers = _numberOfPlayers;
        selectedLevelData = levelContainer.GetGameModeData(mode);
    }
}

public enum GAMEMODE {
    CLASSIC,
    HYPER,
    BOSS
}
