﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player manager works as the spawner for player objects, and as a
/// provider of players data to other game entities. It also sets 
/// palyers properties to initial state.
/// </summary>
public class PlayersManager : MonoBehaviour
{
    public List<GameObject> playerPrefabList;
    public List<PlayerController> instantiatedPlayers;

    //Initialize player manager, spawn players and sets data
    public void InitPlayerManager(int numberOfPlayers) {
        if(instantiatedPlayers.Count > 0) {
            for(int i=0;i<instantiatedPlayers.Count;i++) {
                Destroy(instantiatedPlayers[i].gameObject);
            }
        }

        instantiatedPlayers.Clear();

        if (numberOfPlayers > playerPrefabList.Count) {
            Debug.LogError("Cannot create given number of players, insufficient prefabs");
        }

        for (int i = 0; i < numberOfPlayers; i++) {
            GameObject palyerObj = Instantiate(playerPrefabList[i]);
            PlayerController controller = palyerObj.GetComponent<PlayerController>();
            controller.inputConfig = DataProvider.instance.playersInputConfiguration[i];
            controller.properties = DataProvider.instance.GetPlayerPropertyFor(i);
            controller.properties.playerID = i;
            instantiatedPlayers.Add(controller);         
        }
    }

    //Return a player for given player id
    public PlayerController GetPlayer(int playerID) {
        return instantiatedPlayers[playerID];
    }

    //Checks if all players are dead return true
    public bool AreAllPlayersDead() {
        bool allPlayersDead = true;
        for (int i = 0; i < instantiatedPlayers.Count; i++) {
            if (instantiatedPlayers[i].properties.respawns >= 0) {
                allPlayersDead = false;
                break;
            }
        }
        return allPlayersDead;
    }
}
