﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using SpaceInvadersGameSpace;

/// <summary>
/// Behaviour of player bullet
/// </summary>
public class PlayerBullet : MonoBehaviour {

    //Base player bullet data this is set on the prefab itself
	public PlayerBulletProperties baseBulletProperty;

    //runtime bullet data, this can be re instantiated on every go, and changes will not
    //affect base data
	private PlayerBulletProperties runtimeProperty;
    
    //callback for when the bullet is destroyed
	private UnityAction onDestroyedCallback;
    public bool propertySet = false;

	//Sets shoot data for the give player bullet
	public void Shoot(PlayerProperties shooterProperties, UnityAction _onDestroyedCallback) {
		runtimeProperty = new PlayerBulletProperties() {
			moveSpeed = baseBulletProperty.moveSpeed,
			shooterID = shooterProperties.playerID,
			moveDirection = shooterProperties.shootDirection,
			destroyDelay = baseBulletProperty.destroyDelay
		};
		onDestroyedCallback = _onDestroyedCallback;
        propertySet = true;

        AudioManager.instance.PlayAudio(AUDIOTYPE.PLAYERBULLET);
	}

	//Moves the bullet
	public void Update() {
		if(!propertySet)
			return;
		
		transform.position = new Vector3(
			transform.position.x,
			transform.position.y + (runtimeProperty.moveDirection.y * Time.deltaTime * runtimeProperty.moveSpeed),
			transform.position.z
		);

		if(runtimeProperty.destroyDelay > 0)
			runtimeProperty.destroyDelay -= Time.deltaTime;
		else
			Remove();
	}

    //When collided with enemy set reward for shooter and remove selft
	public void OnCollidedWithEnemy(int reward) {
        GameManager.instance.playerManager.GetPlayer(runtimeProperty.shooterID)
            .properties.score += reward;
        Remove();
	}

	//Remove the bullet, back ready for pooling
	public void Remove() {
        if (!propertySet)
            return;
        propertySet = false;
		onDestroyedCallback();
		onDestroyedCallback = null;
		gameObject.SetActive(false);
	}
}
