﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
/// <summary>
/// Ui handler for GAme en Ui
/// </summary>
public class GameEndUIController : MonoBehaviour
{
    public Text endText;
    public void SetText(string text) {
        endText.text = text;
    }

    public void OnReplayClicked() {
        GameManager.instance.OnReplayClicked();
    }

    public void OnExitClicked() {
        GameManager.instance.LoadLevel("MenuScene");
    }
}
