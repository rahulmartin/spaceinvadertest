﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// UI Group controller for configuration prefab
/// </summary>
public class ConfigGroupController : MonoBehaviour
{
    public List<ConfigUiController> configs;
    public GameObject inputPopupUi;

    //Inits configuration prefabs and sets callbacks for popup
    void Start() {
        for(int i=0;i<configs.Count;i++) {
            configs[i].Init();
            configs[i].RegisterCallbacks(ShowInputPopup, HideInputPopup);
        }
    }

    /// <summary>
    /// shows popup
    /// </summary>
    public void ShowInputPopup() {
        inputPopupUi.gameObject.SetActive(true);
    }

    /// <summary>
    /// hides popup
    /// </summary>
    public void HideInputPopup() {
        inputPopupUi.gameObject.SetActive(false);
    }
}
