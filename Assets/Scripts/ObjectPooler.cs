﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SpaceInvadersGameSpace {
    /// <summary>
    /// Object pooler to pool different objects that are frequently created and
    /// destroyed, this is helpfull optimizing as intantiation and destruction
    /// are heavier tasks, we just enable disable gameobjects here.
    /// </summary>
	public class ObjectPooler : MonoBehaviour {
		public List<ObjectPoolItem> itemsToPool;
		public List<ObjectPoolItem> pooledObjects;

		// Use this for initialization
		public void InitObjectPooler() {
            if (pooledObjects == null)
            {
                InitPooler();
            } else
            {
                ResetPooler();
            }
		}

        //Initializes pooler with defaults
        public void InitPooler()
        {
            pooledObjects = new List<ObjectPoolItem>();
            foreach (ObjectPoolItem item in itemsToPool)
            {
                for (int i = 0; i < item.amountToPool; i++)
                {
                    GameObject newObject = (GameObject)Instantiate(item.objectToPool);
                    ObjectPoolItem pooledItem = new ObjectPoolItem(item, newObject);
                    newObject.SetActive(false);
                    pooledObjects.Add(pooledItem);
                }
            }
        }

        //Resets pooler for next playthough
        public void ResetPooler() {
            foreach (ObjectPoolItem item in pooledObjects) {
                item.objectToPool.SetActive(false);
            }
        }

        //Returns a required pooled object , if unavailable and expandable creates one and returns it
        //otherwise returns null
		public GameObject GetPooledObject(POOLABLES identifier) {
			for(int i = 0; i < pooledObjects.Count; i++) {
				if(!pooledObjects[i].objectToPool.activeInHierarchy && pooledObjects[i].Identifier == identifier) {
					return pooledObjects[i].objectToPool;
				}
			}
			foreach(ObjectPoolItem item in itemsToPool) {
				if(item.Identifier == identifier) {
					if(item.shouldExpand) {
						GameObject newObject = (GameObject)Instantiate(item.objectToPool);
						ObjectPoolItem pooledItem = new ObjectPoolItem(item, newObject);
						newObject.SetActive(false);
						pooledObjects.Add(pooledItem);
						return newObject;
					}
				}
			}
			return null;
		}
	}
		
	[System.Serializable]
	public class ObjectPoolItem {
		public GameObject objectToPool;
		public int amountToPool;
		public bool shouldExpand;
		public POOLABLES Identifier;

		public ObjectPoolItem() {
		}

		public ObjectPoolItem(ObjectPoolItem item, GameObject instantiation) {
			objectToPool = instantiation;
			amountToPool = item.amountToPool;
			shouldExpand = item.shouldExpand;
			Identifier = item.Identifier;
		}
	}

	public enum POOLABLES {
		PLAYERBULLET,
		ENEMYBULLET,
		ENEMY1,
		ENEMY2,
		ENEMY3,
        BLOCKER
	}
}
