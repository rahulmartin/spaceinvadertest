﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvadersGameSpace;

/// <summary>
/// Enemy Ai Controller works as a single brain for all the enemies in scene. This 
/// spawns all enemies in scene, manage thier movement shooting and other behaviour.
/// </summary>
public class EnemyAIController : EnemyControllerType {
    //Initialization Data
    GameObject[,] EnemyMatrix;
    LevelEnemyData levelEnemyData;

    //runtime data
    Vector3 currentMoveDirection = Vector3.right;
    float currentMoveDelay = 0;
    int firedBullets = 0;
    float fireDealy = 0.75f;
    int enemyDestroyed = 0;
    public void Update() {
        //Debug code to find a random enemy
       /* if (Input.GetKeyDown(KeyCode.X)) {
            int randomRow = Random.Range(0, levelEnemyData.enemyPerColumns);
            int randomColumn = Random.Range(0, levelEnemyData.maxColumns);
            EnemyMatrix[randomRow, randomColumn].GetComponent<SpriteRenderer>().color = Color.red;
            Debug.Log(randomRow + "/" + randomColumn);
        }*/


        if (!GameManager.instance.isGameStarted)
            return;

        MoveEnemies();
        ShootProjectile();
    }

    #region Initialization and Resetting of Enemies
    //Init EnemyAI , Spawns enemy from pool set thier data and callback method.
    public override void InitEnemyController() {
        levelEnemyData = DataProvider.instance.GetEnemyData();

        if (EnemyMatrix == null) {
            InitMatrix();
        } else {
            ResetMatrix();
        }
    }

    //Init Matrix, Only once when the game starts
    private void InitMatrix() {
        //TODO;Proper code hre to fetch differnt types of enemies
        int enemyType = 0;
        EnemyMatrix = new GameObject[levelEnemyData.enemyPerColumns, levelEnemyData.maxColumns];
        for (int column = 0; column < levelEnemyData.maxColumns; column++) {
            for (int enemyCount = 0; enemyCount < levelEnemyData.enemyPerColumns; enemyCount++) {
                Vector2Int matrixLocation = new Vector2Int(enemyCount, column);
                GameObject enemy = CreateEnemy(matrixLocation, enemyType);
                if (enemy == null)
                    continue;
                EnemyMatrix[enemyCount, column] = enemy;
                SetEnemyData(new Vector2Int(enemyCount, column), enemy, enemyType);
            }
        }
    }

    //Reset Matrix for a second play though
    private void ResetMatrix() {
        //TODO;Proper code hre to fetch differnt types of enemies
        enemyDestroyed = 0;
        int enemyType = 0;
        for (int column = 0; column < levelEnemyData.maxColumns; column++) {
            for (int enemyCount = 0; enemyCount < levelEnemyData.enemyPerColumns; enemyCount++) {
                GameObject enemy = EnemyMatrix[enemyCount, column];
                SetEnemyData(new Vector2Int(enemyCount, column), enemy, enemyType);
                Vector2Int matrixLocation = new Vector2Int(enemyCount, column);
                SetEnemyPosition(matrixLocation, enemy);
                enemy.SetActive(true);
            }
        }
    }

    //Initializes enemy data
    private void SetEnemyData(Vector2Int enemyLocation, GameObject enemy, int enemyType) {
        EnemyController enemyCtrl = enemy.GetComponent<EnemyController>();
        enemyCtrl.InitEnemy(enemyType,enemyLocation);
        enemyCtrl.RegisterOnKillCallback(OnEnemyKilled);
    }

    //Callback listener method for when an enemy is killed
    public void OnEnemyKilled(Vector2Int _matLoc) {
        enemyDestroyed++;
        if(enemyDestroyed >= EnemyMatrix.Length) {
            GameManager.instance.OnPlayerWon();
        }
    }

    //Create an enemy for a given matrix location and moves the transform to desired location
    public GameObject CreateEnemy(Vector2Int matrixLoc, int enemyType) {

        GameObject currentEnemy = null;


        switch (enemyType) {
            case 0:
                currentEnemy = GameManager.instance.GetPooledObject(SpaceInvadersGameSpace.POOLABLES.ENEMY1);
                break;
            case 1:
                currentEnemy = GameManager.instance.GetPooledObject(SpaceInvadersGameSpace.POOLABLES.ENEMY2);
                break;
            case 2:
                currentEnemy = GameManager.instance.GetPooledObject(SpaceInvadersGameSpace.POOLABLES.ENEMY3);
                break;
            default:
                break;
        }

        if (currentEnemy == null) {
            return currentEnemy;
        }

        SetEnemyPosition(matrixLoc, currentEnemy);
        currentEnemy.SetActive(true);
        return currentEnemy;
    }

    public void SetEnemyPosition(Vector2Int matrixLoc, GameObject currentEnemy) {
        int columnNumber = matrixLoc.y;
        int rowNumber = matrixLoc.x;

        //TODO; minify this
        float xPosition = (DataProvider.instance.GetHorizontalRange() * -1) +
            (columnNumber * levelEnemyData.horizontalDistance) +
            (DataProvider.instance.GetHorizontalRange() -
            levelEnemyData.maxColumns * levelEnemyData.horizontalDistance / 2f) +
            levelEnemyData.horizontalDistance / 2;

        Vector3 worldPosition = new Vector3(xPosition,
            DataProvider.instance.GetVerticalRange() * levelEnemyData.verticalDistance * rowNumber,
            0);

        currentEnemy.transform.position = worldPosition;
    }

    #endregion

    #region Movement Of Enemies
    /// <summary>
    /// Moves enemis alternatively in both direction and vertically on reaching edge
    /// </summary>
    private void MoveEnemies() {
        if (currentMoveDelay <= 0) {
            if (currentMoveDirection == Vector3.right) {
                if (CanMoveRight()) {
                    currentMoveDelay = levelEnemyData.moveTimeDelay;
                    MoveRight();
                } else {
                    if (CanMoveVertical())
                        MoveVertical();
                    else
                        EnemyWonByAssension();

                    currentMoveDirection = Vector3.left;
                }
            }

            if (currentMoveDirection == Vector3.left) {
                if (CanMoveLeft()) {
                    currentMoveDelay = levelEnemyData.moveTimeDelay;
                    MoveLeft();
                } else {
                    if (CanMoveVertical())
                        MoveVertical();
                    else
                        EnemyWonByAssension();

                    currentMoveDirection = Vector3.right;
                }
            }
        }

        currentMoveDelay -= Time.deltaTime;
    }

    /// <summary>
    /// Shoots projectile from the bottom most enemy on randomly selected columns
    /// </summary>
    private void ShootProjectile() {
        if (fireDealy > 0)
            fireDealy -= Time.deltaTime;
        else
        if (CanShoot()) {
            Shoot();
            fireDealy = Random.Range(0, 1f);
        }
    }

    //check if enemies can move right
    private bool CanMoveRight() {
        GameObject rightMostEnemy = GetRightMostEnemy();
        if (rightMostEnemy == null)
            return false;

        //Below is debug code just to show with enemy is on far right
        rightMostEnemy.GetComponent<SpriteRenderer>().color = Color.red;

        if (rightMostEnemy.transform.position.x >= DataProvider.instance.GetHorizontalRange() ||
           rightMostEnemy.transform.position.x + levelEnemyData.horizontalMoveStep > DataProvider.instance.GetHorizontalRange()) {
            return false;
        }

        return true;
    }

    /// <summary>
    /// checks if enemies can move left
    /// </summary>
    /// <returns></returns>
    private bool CanMoveLeft() {
        GameObject leftMostEnemy = GetLeftMostEnemy();
        if (leftMostEnemy == null)
            return false;

        //Below is debug code just to show with enemy is on far left
        leftMostEnemy.GetComponent<SpriteRenderer>().color = Color.yellow;

        if (leftMostEnemy.transform.position.x <= DataProvider.instance.GetHorizontalRange() * -1 ||
           leftMostEnemy.transform.position.x - levelEnemyData.horizontalMoveStep < DataProvider.instance.GetHorizontalRange() * -1) {
            return false;
        }

        return true;
    }

    //Checks if enemies can move vertically
    private bool CanMoveVertical() {
        GameObject bottomMostEnemy = GetBottomMostEnemy();
        if (bottomMostEnemy == null)
            return false;

        //Below is debug code just to show with enemy is on the bottom
        bottomMostEnemy.GetComponent<SpriteRenderer>().color = Color.blue;

        if (bottomMostEnemy.transform.position.y < levelEnemyData.verticalLooseLimit)
            return false;

        return true;
    }

    //Gets an enemy from right most column
    private GameObject GetRightMostEnemy() {
        int rightMostColumn = EnemyMatrix.GetLength(1) - 1;
        GameObject rightMostEnemy = null;
        while (rightMostEnemy == null || rightMostColumn >= 0) {
            for (int i = 0; i < EnemyMatrix.GetLength(0); i++) {
                rightMostEnemy = EnemyMatrix[i, rightMostColumn];
                if (rightMostEnemy) {
                    if (rightMostEnemy.activeInHierarchy)
                        break;
                    else
                        rightMostEnemy = null;
                }
            }
            if (rightMostEnemy)
                break;

            rightMostColumn--;

            if (rightMostColumn < 0)
                break;
        }
        return rightMostEnemy;
    }

    //Gets an enemy from left most column
    private GameObject GetLeftMostEnemy() {
        int leftMostColumn = 0;
        GameObject leftMostEnemy = null;
        while (leftMostEnemy == null || leftMostColumn < EnemyMatrix.GetLength(1)) {
            for (int i = 0; i < EnemyMatrix.GetLength(0); i++) {
                leftMostEnemy = EnemyMatrix[i, leftMostColumn];
                if (leftMostEnemy) {
                    if (leftMostEnemy.activeInHierarchy)
                        break;
                    else
                        leftMostEnemy = null;
                }
            }
            if (leftMostEnemy)
                break;
            leftMostColumn++;


            if (leftMostColumn >= EnemyMatrix.GetLength(1))
                break;
        }
        return leftMostEnemy;
    }

    //Gets the bottom most enemy to check if game was won or if can shoot
    private GameObject GetBottomMostEnemy() {
        int bottomMostRow = 0;
        GameObject bottomMostEnemy = null;
        while (bottomMostEnemy == null || bottomMostRow < EnemyMatrix.GetLength(0)) {
            for(int i=0;i<EnemyMatrix.GetLength(1); i++) {
                bottomMostEnemy = EnemyMatrix[bottomMostRow, i];
                if (bottomMostEnemy) {
                    if (bottomMostEnemy.activeInHierarchy)
                        break;
                    else
                        bottomMostEnemy = null;
                }
            }

            if (bottomMostEnemy)
                break;

            bottomMostRow++;

            if (bottomMostRow >= EnemyMatrix.GetLength(0))
                break;
        }

        return bottomMostEnemy;
    }

    //Moves enemies in right direction
    private void MoveRight() {
        foreach (GameObject obj in EnemyMatrix) {
            obj.transform.position = new Vector3(obj.transform.position.x + levelEnemyData.horizontalMoveStep,
                obj.transform.position.y,
                obj.transform.position.z);
        }
    }

    //Moves enemies in left direction
    private void MoveLeft() {
        foreach (GameObject obj in EnemyMatrix) {
            obj.transform.position = new Vector3(obj.transform.position.x - levelEnemyData.horizontalMoveStep,
                obj.transform.position.y,
                obj.transform.position.z);
        }
    }

    private void MoveVertical() {
        foreach (GameObject obj in EnemyMatrix) {
            obj.transform.position = new Vector3(obj.transform.position.x,
                obj.transform.position.y - levelEnemyData.verticalMoveStep,
                obj.transform.position.z);
        }
    }


    #endregion

    #region EnemyShooting system
    //Checks if shooting is valid
    public bool CanShoot() {
        if (Random.Range(0, 3) == 1)
            return false;
        return firedBullets < levelEnemyData.fireRate;
    }

    //Shoots a projectile if available
    public void Shoot() {
        GameObject selectedEnemy = GetEnemyToShoot();

        if (selectedEnemy == null) 
            return;
        
        GameObject bullet = GameManager.instance.GetPooledObject(POOLABLES.ENEMYBULLET);
        if (bullet == null) 
            return;
        

        EnemyBullet bulletController = bullet.GetComponent<EnemyBullet>();
        bullet.transform.position = selectedEnemy.transform.position;
        bulletController.Shoot(OnBulletDestroyed);
        bullet.SetActive(true);
        firedBullets++;
    }

    //callback when a previously bullet is destroyed
    public void OnBulletDestroyed() {
        firedBullets--;
    }

    //Finds a valid enemy that can shoot a projectile
    public GameObject GetEnemyToShoot() {
        List<GameObject> validEnemies = new List<GameObject>();

        for(int i=0; i<EnemyMatrix.GetLength(1); i++) {
            for(int row = 0; row < EnemyMatrix.GetLength(0); row++) {
                GameObject selectedEnemy = null;
                if (EnemyMatrix[row, i].activeInHierarchy) {
                    selectedEnemy = EnemyMatrix[row, i];
                    validEnemies.Add(selectedEnemy);
                    break;
                }     
            }
        }

        if (validEnemies.Count <= 0)
            return null;

        int selectedIndex = Random.Range(0, validEnemies.Count);
        return validEnemies[selectedIndex];
    }

    #endregion

    #region WinLooseCallback
    //method to call when an enemy has landed
    private void EnemyWonByAssension() {
        GameManager.instance.OnEnemyWonByAssension();
    }

    private void OnPlayerWon() {
        GameManager.instance.OnPlayerWon();
    }

    public override void OnGameEnded() {
    }
    #endregion

}

namespace SpaceInvadersGameSpace {
    //Abstract class for enemycontroller , so that boss recieves the same
    //init command as the level Ai Controller
    public abstract class EnemyControllerType : MonoBehaviour {
        public abstract void InitEnemyController();
        public abstract void OnGameEnded();
    }
}
