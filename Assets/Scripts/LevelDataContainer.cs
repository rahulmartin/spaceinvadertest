﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvadersGameSpace;
/// <summary>
/// This asset file will have a complete data configuration for a level.
/// it have level data for number of enemies to create, enemies properties,
/// player properties overrides for a given level, different game mode settings
/// </summary>
[CreateAssetMenu(fileName = "NewLevelData", menuName = "LevelData")]
public class LevelDataContainer : ScriptableObject
{
    public List<LevelData> Levels;

    public LevelData GetGameModeData(GAMEMODE mode) {
        LevelData returnData = new LevelData();
        for(int i=0; i<Levels.Count; i++) {
            if(Levels[i].gameMode == mode) {
                returnData = Levels[i];
                break;
            }
        }

        return returnData;
    }
}

[System.Serializable]
public class LevelData {
    public GAMEMODE gameMode;
    public LevelEnemyData enemyData;
    public List<EnemyProperties> enemyPropertyList;
    public List<PlayerProperties> playerProperties;
}
