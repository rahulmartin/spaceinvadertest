﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public List<AudioItem> audioList = new List<AudioItem>();
    public AudioSource source;
    public AudioSource musicSource;

    // Setting up the singleton
    void Awake() {
        if (instance != null)
            DestroyImmediate(this);
        else {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void PlayAudio(AUDIOTYPE type) {
        AudioClip clip = null;
        for(int i=0; i<audioList.Count; i++) {
            if(audioList[i].type == type) {
                clip = audioList[i].audio;
                break;
            }
        }
        source.clip = clip;
        source.Play();
    }

    public void PlayMusic(AUDIOTYPE type) {
        AudioClip clip = null;
        for (int i = 0; i < audioList.Count; i++) {
            if (audioList[i].type == type) {
                clip = audioList[i].audio;
                break;
            }
        }
        musicSource.clip = clip;
        musicSource.loop = true;
        musicSource.volume = 1f;
        musicSource.Play();
    }
}

[System.Serializable]
public class AudioItem {
    public AudioClip audio;
    public AUDIOTYPE type;
}

public enum AUDIOTYPE {
    ENEMYBULLET,
    PLAYERBULLET,
    ENEMYHIT,
    PLAYERHIT,
    MUSIC
}
