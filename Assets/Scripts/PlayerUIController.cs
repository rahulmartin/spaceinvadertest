﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A Parent controller for setting up initial data on player score uis
/// </summary>
public class PlayerUIController : MonoBehaviour
{
    public List<PlayerScoreUi> playerScoreUiList;

    public void SetUpUiFor(int numberOfPlayers) {
        for (int i=0; i<playerScoreUiList.Count; i++) {
            if (i < numberOfPlayers) {
                playerScoreUiList[i].gameObject.SetActive(true);
                playerScoreUiList[i].Init(i);
            }
        }
    }
}
