﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossConnector : MonoBehaviour
{
    BossController bossCtrl;
    //When a Player Bullet collides with this forward this collision to Bosscontroller
    void OnTriggerEnter2D(Collider2D col) {
        if(bossCtrl == null)
            bossCtrl = GameManager.instance.enemyController as BossController;

        bossCtrl.OnBulletCollided(col);
    }
}
