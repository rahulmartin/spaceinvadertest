﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SpaceInvadersGameSpace;
using UnityEngine.Events;

/// <summary>
/// Ui to help player change key configuration from main menu
/// </summary>
public class ConfigUiController : MonoBehaviour
{
    public Button left;
    public Button right;
    public Button shoot;
    public int PlayerID;
    bool isWaitingForInput;
    int WaitingForKey = -1;

    UnityAction showPopupCallback;
    UnityAction hidePopupCallback;

    //Check for key strokes after player has already selected the key to change
    public void Update() {
        if (!isWaitingForInput)
            return;

        //Hack here again :(
        foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode))) {
            if (Input.GetKey(kcode)) {
                isWaitingForInput = false;
                switch(WaitingForKey) {
                    case 0:
                        DataProvider.instance.playersInputConfiguration[PlayerID].leftKey = kcode;
                        break;
                    case 1:
                        DataProvider.instance.playersInputConfiguration[PlayerID].rightKey = kcode;
                        break;
                    case 2:
                        DataProvider.instance.playersInputConfiguration[PlayerID].shootKey = kcode;
                        break;
                    default:
                        Debug.Log("WAiing for wrong key" + WaitingForKey);
                        break;
                }
                Init();
            }
        }
    }

    //Initialize the config ui for all given players
    public void Init() {
        InputConfig config = DataProvider.instance.playersInputConfiguration[PlayerID];
        left.GetComponentInChildren<Text>().text = config.leftKey.ToString();
        right.GetComponentInChildren<Text>().text = config.rightKey.ToString();
        shoot.GetComponentInChildren<Text>().text = config.shootKey.ToString();
        if(hidePopupCallback != null)
        hidePopupCallback();
    }

    //callback to show hide ui popup
    public void RegisterCallbacks(UnityAction showCB, UnityAction hideCB) {
        showPopupCallback = showCB;
        hidePopupCallback = hideCB;
    }

    //On left button clicked on the ui prefab
    public void OnLeftClicked() {
        isWaitingForInput = true;
        WaitingForKey = 0;
        showPopupCallback();
    }

    //callbakc for right button change command
    public void OnRightClicked() {
        isWaitingForInput = true;
        WaitingForKey = 1;
        showPopupCallback();
    }

    /// <summary>
    /// callback for shoot button chang ecommand
    /// </summary>
    public void OnShootClicked() {
        isWaitingForInput = true;
        WaitingForKey = 2;
        showPopupCallback();
    }
}
